package com.example.adidascodechallenge.data.models

data class ReviewData(
    val locale: String,
    val productId: String,
    val rating: Int,
    val text: String
)