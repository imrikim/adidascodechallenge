package com.example.adidascodechallenge.data.models

import com.example.adidascodechallenge.presentation.common.shared.Constants

class ProductData {
    var currency : String = Constants.Global.EMPTY
    var description : String = Constants.Global.EMPTY
    var id : String = Constants.Global.EMPTY
    var imgUrl : String = Constants.Global.EMPTY
    var name : String = Constants.Global.EMPTY
    var price: Int = 0
    var reviews: List<ReviewData> = ArrayList()
}

