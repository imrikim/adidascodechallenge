package com.example.adidascodechallenge.data.callbacks

interface ApiCallback <M> {

    fun onResponse(data : M)

    fun onFailure()
}