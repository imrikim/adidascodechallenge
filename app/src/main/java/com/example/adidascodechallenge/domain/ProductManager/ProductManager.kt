package com.example.adidascodechallenge.domain.ProductManager

import android.annotation.SuppressLint
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.adidascodechallenge.data.callbacks.ApiCallback
import com.example.adidascodechallenge.data.models.ProductData
import com.example.adidascodechallenge.data.models.ReviewData
import com.example.adidascodechallenge.presentation.common.application.AdidasChallengeApp
import com.example.adidascodechallenge.presentation.common.shared.Constants
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class ProductManager : IProductManager {

    override var selectedProductData = ProductData()
    override var allProductData: ArrayList<ProductData> = ArrayList()


    @SuppressLint("ShowToast")

    /** This method is used to fetch all products details available see [IProductManager.fetchAllProducts]**/
    override fun fetchAllProducts(callback: ApiCallback<List<ProductData>>) {
        var listOfProducts = ArrayList<ProductData>()
        val queue = Volley.newRequestQueue(AdidasChallengeApp.instance)
        val url = "http://${Constants.Global.machineAddressIp}:3001/product"
        val stringRequest = StringRequest(
                Request.Method.GET, url, Response.Listener<String> {
                    response ->
                val gson = Gson()
                val collectionType: Type = object : TypeToken<Collection<ProductData?>?>() {}.type
                val products: Collection<ProductData> = gson.fromJson(response, collectionType)
                listOfProducts = products as ArrayList<ProductData>
                allProductData.clear()
                allProductData.addAll(listOfProducts)
                callback.onResponse(listOfProducts)
            },
                Response.ErrorListener {
                    callback.onFailure()
                    Toast.makeText(AdidasChallengeApp.instance, "no data are available", Toast.LENGTH_SHORT)
                })
        queue.add(stringRequest)
    }

    /** This method is used to fetch product details see [IProductManager.fetchProductById]**/
    override fun fetchProductById(productId: String?, callback: ApiCallback<ProductData>) {
        var product : ProductData
        val queue = Volley.newRequestQueue(AdidasChallengeApp.instance)
        val url = "http://${Constants.Global.machineAddressIp}:3001/product/$productId"
        val stringRequest = StringRequest(Request.Method.GET, url, Response.Listener<String> {
                    response ->
                val gson = Gson()
                product = gson.fromJson(response, ProductData::class.java)
                callback.onResponse(product)
            },
                Response.ErrorListener {
                    it.networkResponse
                    callback.onFailure()
                })
        queue.add(stringRequest)
    }

    /** This method is used to post a review on a product see [IProductManager.postReview]**/
    override fun postReview(productId: String?, review: String, rating: String, callback: ApiCallback<Any>) {
        val url = "http://${Constants.Global.machineAddressIp}:3002/reviews/$productId"
        val queue = Volley.newRequestQueue(AdidasChallengeApp.instance)
        val postRequest: StringRequest = object : StringRequest(Method.POST, url,
            Response.Listener { response -> // response
                Log.d("Response", response)
                callback.onResponse(response)
            },
            Response.ErrorListener { // error
                it.networkResponse
                callback.onFailure()
            }
        ) {
            override fun getParams(): Map<String, String>? {
                val params: MutableMap<String, String> =
                    HashMap()
                params["rating"] = rating
                params["text"] = review
                return params
            }
        }
        queue.add(postRequest)
    }

    /** This method is used to fetch all reviews on a product see [IProductManager.fetchAllReviews]**/
    override fun fetchAllReviews(productId : String?, callback: ApiCallback<List<ReviewData>>) {
        var listOfReviews = ArrayList<ReviewData>()
        val queue = Volley.newRequestQueue(AdidasChallengeApp.instance)
        val url = "http://${Constants.Global.machineAddressIp}:3002/reviews/$productId"
        val stringRequest = StringRequest(
            Request.Method.GET, url, Response.Listener<String> {
                    response ->
                val gson = Gson()
                val collectionType: Type = object : TypeToken<Collection<ReviewData?>?>() {}.type
                val products: Collection<ReviewData> = gson.fromJson(response, collectionType)
                listOfReviews = products as ArrayList<ReviewData>
                callback.onResponse(listOfReviews)
            },
            Response.ErrorListener {
                callback.onFailure()
                Toast.makeText(AdidasChallengeApp.instance, "no data are available", Toast.LENGTH_SHORT)
            })
        queue.add(stringRequest)
    }


}