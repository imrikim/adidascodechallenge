package com.example.adidascodechallenge.domain.ProductManager

import com.example.adidascodechallenge.data.callbacks.ApiCallback
import com.example.adidascodechallenge.data.models.ProductData
import com.example.adidascodechallenge.data.models.ReviewData

interface IProductManager {

    var selectedProductData : ProductData

    var allProductData : ArrayList<ProductData>

    /** This method is used to fetch all products details available see [ProductManager.fetchAllProducts]**/
    fun fetchAllProducts(callback: ApiCallback<List<ProductData>>)

    /** This method is used to fetch product details see [ProductManager.fetchProductById]**/
    fun fetchProductById(productId: String?, callback: ApiCallback<ProductData>)

    /** This method is used to post a review on a product see [ProductManager.postReview]**/
    fun postReview(productId: String?, review: String, rating: String, callback: ApiCallback<Any>)

    /** This method is used to fetch all reviews on a product see [ProductManager.fetchAllReviews]**/
    fun fetchAllReviews(productId: String?, callback: ApiCallback<List<ReviewData>>)
}