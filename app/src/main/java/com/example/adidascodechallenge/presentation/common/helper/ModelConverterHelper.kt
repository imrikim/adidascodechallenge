package com.example.adidascodechallenge.presentation.common.helper

import com.example.adidascodechallenge.data.models.ProductData
import com.example.adidascodechallenge.data.models.ReviewData
import com.example.adidascodechallenge.presentation.common.bindingModels.ProductBindingModel
import com.example.adidascodechallenge.presentation.common.bindingModels.ReviewBindingModel

class ModelConverterHelper {
    companion object {

        @JvmStatic
        fun convertProductsDataToProductBindingModel(productsData : List<ProductData>) : List<ProductBindingModel> {
            var listProductBindingModel = ArrayList<ProductBindingModel>()
            for (productData in productsData) {
                listProductBindingModel.add(convertProductDataToProductBindingModel(productData))
            }
            return listProductBindingModel
        }

        @JvmStatic
        fun convertProductDataToProductBindingModel(productData: ProductData) : ProductBindingModel{
            val productBindingModel = ProductBindingModel()
            productBindingModel.id = productData.id
            productBindingModel.productName = productData.name
            productBindingModel.productDescription = productData.description
            productBindingModel.productGroup = productData.name
            productBindingModel.productPrice = productData.price.toString()+productData.currency
            productBindingModel.productDrawable = productData.imgUrl

            return productBindingModel
        }
        @JvmStatic
        fun convertReviewsDataToReviewBindingModel(reviewsData : List<ReviewData>) : List<ReviewBindingModel> {
            var listReviewBindingModel = ArrayList<ReviewBindingModel>()
            for (reviewData in reviewsData) {
                listReviewBindingModel.add(convertReviewDataToReviewBindingModel(reviewData))
            }
            return listReviewBindingModel
        }

        @JvmStatic
        fun convertReviewDataToReviewBindingModel(reviewData: ReviewData) : ReviewBindingModel{
            val reviewBindingModel = ReviewBindingModel()
            reviewBindingModel.reviewText = reviewData.text
            reviewBindingModel.rating = reviewData.rating.toString()

            return reviewBindingModel
        }
    }

}