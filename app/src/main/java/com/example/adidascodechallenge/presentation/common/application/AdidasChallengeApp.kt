package com.example.adidascodechallenge.presentation.common.application

import android.app.Application
import com.example.adidascodechallenge.domain.ProductManager.IProductManager
import com.example.adidascodechallenge.domain.ProductManager.ProductManager
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.dsl.module

class AdidasChallengeApp : Application(){

    val appModule = module {
        single<IProductManager> {
            ProductManager()
        }
    }
    companion object {
        lateinit var instance : AdidasChallengeApp
    }

    override fun onCreate() {
        instance = this
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@AdidasChallengeApp)
            modules(appModule)
        }
    }
}