package com.example.adidascodechallenge.presentation.features.splashScreen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.example.adidascodechallenge.R
import com.example.adidascodechallenge.presentation.features.MainActivity

class SplashActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container,
                    SplashFragment.newInstance()
                )
                .commitNow()
            navigateToNextActivity()
        }    }

    private fun navigateToNextActivity() {
        Handler().postDelayed({
            val intent = Intent(this, MainActivity::class.java)
            this.startActivity(intent)
            this.finish()
        }, 3000)
    }
}