package com.example.adidascodechallenge.presentation.common.shared;

import androidx.databinding.BindingAdapter;

import android.view.View;
public class AndroidDataBindingAdapters {


    @BindingAdapter("visibleElseGone")
    public static void visible(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("visibleElseInvisible")
    public static void invisible(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

}