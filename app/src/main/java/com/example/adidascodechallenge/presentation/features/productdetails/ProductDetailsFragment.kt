package com.example.adidascodechallenge.presentation.features.productdetails

import com.example.adidascodechallenge.R
import com.example.adidascodechallenge.presentation.common.fragment.BaseFragment

class ProductDetailsFragment : BaseFragment<ProductDetailsViewModel>(ProductDetailsViewModel(), R.layout.product_details_fragment) {

}