package com.example.adidascodechallenge.presentation.common.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.adidascodechallenge.presentation.common.activity.BaseActivity
import com.example.adidascodechallenge.presentation.common.viewModel.BaseViewModel

abstract class BaseFragment<VM : BaseViewModel<*>>(var viewModel: VM, var view: Int) : Fragment() {

    fun navigateWithIdDestination(idDestination: Int) {
        findNavController().navigate(idDestination, null)
    }

    fun navigateUp() {
        findNavController().navigateUp()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val root = inflater.inflate(this.view, container, false)
        viewModel.configDataBinding(DataBindingUtil.bind(root))
        return root
    }

    override fun onAttach(context: Context?) {
        if (activity != null)
            viewModel.activity = activity!! as BaseActivity
        viewModel.fragment = this

        super.onAttach(context)
    }
}