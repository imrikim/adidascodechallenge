package com.example.adidascodechallenge.presentation.common.viewModel

import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.example.adidascodechallenge.presentation.common.activity.BaseActivity
import com.example.adidascodechallenge.presentation.common.fragment.BaseFragment
import org.koin.core.KoinComponent

abstract class BaseViewModel<VB : ViewDataBinding> : ViewModel(), KoinComponent {

    private lateinit var viewDataBinding: VB

    lateinit var activity : BaseActivity

    lateinit var fragment: BaseFragment<*>

    fun configDataBinding(viewDataBinding: ViewDataBinding?){
        if (viewDataBinding != null) {
            this.viewDataBinding = viewDataBinding as VB
            setupDataBinding(viewDataBinding)
        }
    }

    abstract fun setupDataBinding(binding: VB)

}