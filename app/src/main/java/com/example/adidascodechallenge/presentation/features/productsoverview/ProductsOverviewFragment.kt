package com.example.adidascodechallenge.presentation.features.productsoverview

import com.example.adidascodechallenge.R
import com.example.adidascodechallenge.presentation.common.fragment.BaseFragment

class ProductsOverviewFragment : BaseFragment<ProductsOverviewViewModel>(ProductsOverviewViewModel(), R.layout.product_overview_fragment) {

    override fun onResume() {
        viewModel.fetchAllProducts()
        super.onResume()
    }
}