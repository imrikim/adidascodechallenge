package com.example.adidascodechallenge.presentation.features.productsoverview

import com.example.adidascodechallenge.R
import com.example.adidascodechallenge.data.callbacks.ApiCallback
import com.example.adidascodechallenge.data.models.ProductData
import com.example.adidascodechallenge.databinding.ProductOverviewFragmentBinding
import com.example.adidascodechallenge.domain.ProductManager.IProductManager
import com.example.adidascodechallenge.presentation.common.bindingModels.ProductBindingModel
import com.example.adidascodechallenge.presentation.common.helper.ModelConverterHelper
import com.example.adidascodechallenge.presentation.common.viewModel.BaseViewModel
import org.koin.core.get

class ProductsOverviewViewModel : BaseViewModel<ProductOverviewFragmentBinding>() {

    var productManager : IProductManager = get()

    var bindingModel = ProductsOverviewBindingModel()

    override fun setupDataBinding(binding : ProductOverviewFragmentBinding) {
        bindingModel.listViewAdapter = ProductOverviewAdapter(this, activity)
        binding.viewModel = this
        fetchAllProducts()
    }

    fun fetchAllProducts() {

        productManager.fetchAllProducts(object : ApiCallback<List<ProductData>>{
            override fun onResponse(data: List<ProductData>) {
                bindingModel.listIsEmpty = false
                bindingModel.listOfProducts = ModelConverterHelper.convertProductsDataToProductBindingModel(data)
                bindingModel.listViewAdapter!!.updateList(ModelConverterHelper.convertProductsDataToProductBindingModel(data))
            }

            override fun onFailure() {
                bindingModel.listIsEmpty = true
                println("message adidas on Failure")
            }
        })
    }

    fun navigateToProductDetails(productBindingModel: ProductBindingModel) {
        productManager.selectedProductData = productManager.allProductData.find { it.id == productBindingModel.id }!!
        fragment.navigateWithIdDestination(R.id.productFragment)
    }

    fun refreshProductOverview() {
        fetchAllProducts()
        bindingModel.isSwipeViewRefreshing = false
    }


}