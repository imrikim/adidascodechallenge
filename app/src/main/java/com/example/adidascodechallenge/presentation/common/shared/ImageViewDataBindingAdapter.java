package com.example.adidascodechallenge.presentation.common.shared;

import androidx.databinding.BindingAdapter;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.example.adidascodechallenge.R;
import com.squareup.picasso.Picasso;


/**
 * Custom Adapters for {@link ImageView}
 *
 * @author Mrikim Ilyas <imrikim@itemis.de>
 */

public class ImageViewDataBindingAdapter {

    // Blinking Image
    @BindingAdapter("blink")
    public static void setBlink(ImageView imageView, boolean value) {
        if (value) {
            Animation animation = new AlphaAnimation(1, 0);
            animation.setDuration(1000);
            animation.setInterpolator(new LinearInterpolator());
            animation.setRepeatCount(Animation.INFINITE);
            animation.setRepeatMode(Animation.REVERSE);
            imageView.startAnimation(animation);
        }
    }

    @BindingAdapter("imageUrl")
    public  static void setImageFromUrl(ImageView imageView,String urlString) {
        if (!urlString.isEmpty()) {
            Picasso.get().load(urlString).into(imageView);
        } else {
            imageView.setBackgroundResource(R.drawable.adidas_app_logo);
        }
    }
}
