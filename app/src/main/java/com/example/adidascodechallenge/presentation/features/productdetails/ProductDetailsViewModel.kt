package com.example.adidascodechallenge.presentation.features.productdetails

import com.example.adidascodechallenge.data.callbacks.ApiCallback
import com.example.adidascodechallenge.data.models.ProductData
import com.example.adidascodechallenge.data.models.ReviewData
import com.example.adidascodechallenge.databinding.ProductDetailsFragmentBinding
import com.example.adidascodechallenge.domain.ProductManager.IProductManager
import com.example.adidascodechallenge.presentation.common.helper.ModelConverterHelper.Companion.convertProductDataToProductBindingModel
import com.example.adidascodechallenge.presentation.common.helper.ModelConverterHelper.Companion.convertReviewsDataToReviewBindingModel
import com.example.adidascodechallenge.presentation.common.viewModel.BaseViewModel
import org.koin.core.get

class ProductDetailsViewModel : BaseViewModel<ProductDetailsFragmentBinding>(){

    var productManager : IProductManager = get()

    var bindingModel = ProductDetailsBindingModel()

    override fun setupDataBinding(binding : ProductDetailsFragmentBinding) {
        bindingModel.listOfReviewsAdapter = ListReviewsAdapter(this, activity)
        binding.viewModel = this
        fetchProductById()
    }

    private fun fetchProductById() {
        productManager.fetchProductById(productManager.selectedProductData.id, object : ApiCallback<ProductData>{
            override fun onResponse(data: ProductData) {
                bindingModel.product = convertProductDataToProductBindingModel(data)
                bindingModel.listOfReviews = data.reviews as ArrayList<ReviewData>
                bindingModel.noConnection = false
                bindingModel.listOfReviewsAdapter!!.updateList(convertReviewsDataToReviewBindingModel(data.reviews))
                productManager.fetchAllReviews(bindingModel.product.id, object : ApiCallback<List<ReviewData>>{
                    override fun onResponse(data: List<ReviewData>) {
                        val allReviews = ArrayList<ReviewData>()
                        allReviews.addAll(bindingModel.listOfReviews)
                        allReviews.addAll(data)
                        bindingModel.noConnection = false
                        bindingModel.listOfReviewsAdapter!!.updateList(convertReviewsDataToReviewBindingModel(allReviews))
                    }

                    override fun onFailure() {
                        println("message adidas on Failure")
                        bindingModel.noConnection = true
                    }

                })
            }

            override fun onFailure() {
                println("message adidas on Failure")
                bindingModel.noConnection = true
            }

        })
    }

    fun navigateBack() {
        fragment.navigateUp()
    }

    fun addReview() {
        productManager.postReview(bindingModel.product.id,bindingModel.review,bindingModel.rating, object : ApiCallback<Any>{
            override fun onResponse(data: Any) {
                productManager.fetchAllReviews(bindingModel.product.id, object : ApiCallback<List<ReviewData>>{
                    override fun onResponse(data: List<ReviewData>) {
                        val allReviews = ArrayList<ReviewData>()
                        allReviews.addAll(bindingModel.listOfReviews)
                        allReviews.addAll(data)
                        bindingModel.noConnection = false
                        bindingModel.listOfReviewsAdapter!!.updateList(convertReviewsDataToReviewBindingModel(allReviews))
                    }

                    override fun onFailure() {
                        println("message adidas on Failure")
                        bindingModel.noConnection = true
                    }
                })
            }

            override fun onFailure() {
                println("message adidas on Failure")
                bindingModel.noConnection = true
            }

        })
    }

    fun showBottomSheetCreateRoom() {
        BottomActionSheetDialog.showBottomSheetCreateRoom(fragment.context, this)
    }

}