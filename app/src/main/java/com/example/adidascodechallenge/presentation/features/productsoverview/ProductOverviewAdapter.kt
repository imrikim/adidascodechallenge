package com.example.adidascodechallenge.presentation.features.productsoverview

import android.content.Context
import androidx.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.adidascodechallenge.R
import com.example.adidascodechallenge.databinding.ListItemProductBinding
import com.example.adidascodechallenge.presentation.common.bindingModels.ProductBindingModel

class ProductOverviewAdapter(private var productsOverviewViewModel: ProductsOverviewViewModel, private var context: Context?) : BaseAdapter() {

    private val productBindingModels: ArrayList<ProductBindingModel> = ArrayList()
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int = productBindingModels.size

    override fun getItem(position: Int): ProductBindingModel = productBindingModels[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var binding:  ListItemProductBinding? = null
        if (convertView != null) binding = DataBindingUtil.getBinding(convertView)
        if (binding == null) binding = DataBindingUtil.inflate(inflater, R.layout.list_item_product, parent, false)

        val productModel = getItem(position)

        binding!!.model = productModel
        binding.viewModel = productsOverviewViewModel

        binding.executePendingBindings()
        return binding.root
    }

    fun updateList(listProductBindings: List<ProductBindingModel>?) {
        if (listProductBindings != null) {
            productBindingModels.clear()
            productBindingModels.addAll(listProductBindings.sortedBy { it.productName })
            notifyDataSetChanged()
        }
    }

    fun clear() {
        productBindingModels.clear()
        notifyDataSetChanged()
    }

}