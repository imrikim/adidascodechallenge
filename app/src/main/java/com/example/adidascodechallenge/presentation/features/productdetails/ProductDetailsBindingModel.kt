package com.example.adidascodechallenge.presentation.features.productdetails

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.example.adidascodechallenge.data.models.ReviewData
import com.example.adidascodechallenge.presentation.common.bindingModels.ProductBindingModel
import com.example.adidascodechallenge.presentation.common.shared.Constants
import java.util.*

class  ProductDetailsBindingModel: BaseObservable() {

    @get:Bindable
    var listOfReviewsAdapter : ListReviewsAdapter? = null
        set(listOfReviewsAdapter) {
                field = listOfReviewsAdapter
                notifyPropertyChanged(BR.listOfReviewsAdapter)

        }


    @get:Bindable
    var noConnection = true
        set(noConnection) {
            field = noConnection
            notifyPropertyChanged(BR.noConnection)
        }

    @get:Bindable
    var isSwipeViewRefreshing: Boolean = false
        set(swipeViewRefreshing) {
            field = swipeViewRefreshing
            notifyPropertyChanged(BR.swipeViewRefreshing)
        }

    @get:Bindable
    var listOfReviews = ArrayList<ReviewData>()
        set(listOfReviews) {
            field = listOfReviews
            notifyPropertyChanged(BR.listOfReviews)
        }

    @get:Bindable
    var review: String = Constants.Global.EMPTY
        set(review) {
            field = review
            notifyPropertyChanged(BR.review)
        }

    @get:Bindable
    var rating: String = Constants.Global.EMPTY
        set(rating) {
            field = rating
            notifyPropertyChanged(BR.rating)
        }


    @get:Bindable
    var product : ProductBindingModel = ProductBindingModel()
        set(product) {
            field = product
            notifyPropertyChanged(BR.product)
        }

}