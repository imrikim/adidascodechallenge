package com.example.adidascodechallenge.presentation.features.productdetails

import android.content.Context
import androidx.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.adidascodechallenge.R
import com.example.adidascodechallenge.databinding.ListItemReviewBinding
import com.example.adidascodechallenge.presentation.common.bindingModels.ReviewBindingModel

class ListReviewsAdapter(private var productDetailsViewModel: ProductDetailsViewModel, private var context: Context?) : BaseAdapter() {

    private val reviewModels: ArrayList<ReviewBindingModel> = ArrayList()
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int = reviewModels.size

    override fun getItem(position: Int): ReviewBindingModel = reviewModels[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var binding: ListItemReviewBinding? = null
        if (convertView != null) binding = DataBindingUtil.getBinding(convertView)
        if (binding == null) binding = DataBindingUtil.inflate(inflater, R.layout.list_item_review, parent, false)

        val reviewBindingModel = getItem(position)

        binding!!.model = reviewBindingModel
        binding.executePendingBindings()
        return binding.root
    }

    fun updateList(reviews: List<ReviewBindingModel>?) {
        if (reviews != null) {
            reviewModels.clear()
            reviewModels.addAll(reviews)
            notifyDataSetChanged()
        }
    }

    fun clear() {
        reviewModels.clear()
        notifyDataSetChanged()
    }
}
