package com.example.adidascodechallenge.presentation.features.productdetails

import android.content.Context
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialog
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import com.example.adidascodechallenge.R
import com.example.adidascodechallenge.databinding.DialogReviewingBinding
import com.example.adidascodechallenge.presentation.common.shared.Constants

class BottomActionSheetDialog {

    companion object {
        @JvmStatic
        fun showBottomSheetCreateRoom(activity: Context?, detailsViewModel: ProductDetailsViewModel) {
            val inflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val binding: DialogReviewingBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_reviewing, null, false)
            binding.viewModel = detailsViewModel
            val bottomSheetDialog =
                BottomSheetDialog(
                    activity,
                    R.style.AppBottomSheetDialogTheme
                )
            bottomSheetDialog.setContentView(binding.root)
            bottomSheetDialog.setCancelable(true)
            bottomSheetDialog.show()
            binding.rating.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(text: CharSequence, start: Int, count: Int, after: Int) {
                    detailsViewModel.bindingModel.rating = text.toString()
                }

                override fun onTextChanged(text: CharSequence, start: Int, before: Int, count: Int) {
                    detailsViewModel.bindingModel.rating = text.toString()
                }

                override fun afterTextChanged(text: Editable) {
                    detailsViewModel.bindingModel.rating = text.toString()
                }
            })
            binding.review.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(text: CharSequence, start: Int, count: Int, after: Int) {
                    detailsViewModel.bindingModel.review = text.toString()
                }

                override fun onTextChanged(text: CharSequence, start: Int, before: Int, count: Int) {
                    detailsViewModel.bindingModel.review = text.toString()
                }

                override fun afterTextChanged(text: Editable) {
                    detailsViewModel.bindingModel.review = text.toString()
                }
            })
            binding.btnAddReview.setOnClickListener { _ ->
                bottomSheetDialog.dismiss()
                if (!detailsViewModel.bindingModel.rating.isNullOrEmpty() && !detailsViewModel.bindingModel.review.isNullOrEmpty()  ) {
                    detailsViewModel.addReview()
                }
                detailsViewModel.bindingModel.review = Constants.Global.EMPTY
                detailsViewModel.bindingModel.rating = Constants.Global.EMPTY
            }
            binding.btnCancelReview.setOnClickListener { _ ->
                bottomSheetDialog.dismiss()
                detailsViewModel.bindingModel.review = Constants.Global.EMPTY
                detailsViewModel.bindingModel.rating = Constants.Global.EMPTY
            }
        }

    }

}