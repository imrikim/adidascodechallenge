package com.example.adidascodechallenge.presentation.common.bindingModels

import com.example.adidascodechallenge.presentation.common.shared.Constants

class ReviewBindingModel {
    var reviewText = Constants.Global.EMPTY
    var rating = Constants.Global.EMPTY
}
   
