package com.example.adidascodechallenge.presentation.common.bindingModels

import com.example.adidascodechallenge.presentation.common.shared.Constants

class ProductBindingModel {
    var id = Constants.Global.EMPTY
    var productName = Constants.Global.EMPTY
    var productDescription = Constants.Global.EMPTY
    var productGroup = Constants.Global.EMPTY
    var productPrice = Constants.Global.EMPTY
    var productDrawable = Constants.Global.EMPTY
}