package com.example.adidascodechallenge.presentation.common.shared

class Constants {
    object Global {
        const val EMPTY = ""
        const val machineAddressIp = "192.168.1.8"
    }

}