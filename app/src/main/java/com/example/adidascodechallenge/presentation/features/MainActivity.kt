package com.example.adidascodechallenge.presentation.features

import android.os.Bundle
import com.example.adidascodechallenge.R
import com.example.adidascodechallenge.presentation.common.activity.BaseActivity


class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }
}