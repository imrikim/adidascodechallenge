package com.example.adidascodechallenge.presentation.features.productsoverview

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.example.adidascodechallenge.presentation.common.bindingModels.ProductBindingModel
import com.example.adidascodechallenge.presentation.common.shared.Constants

class ProductsOverviewBindingModel : BaseObservable() {

    @get:Bindable
    var listViewAdapter: ProductOverviewAdapter? = null
        set(listViewAdapter) {
            field = listViewAdapter
            notifyPropertyChanged(BR.listViewAdapter)
        }

    @get:Bindable
    var listOfProducts : List<ProductBindingModel> = ArrayList()


    @get:Bindable
    var listIsEmpty = true
        set(listIsEmpty) {
            field = listIsEmpty
            notifyPropertyChanged(BR.listIsEmpty)
        }

    @get:Bindable
    var searchBar: String = Constants.Global.EMPTY
        set(searchBar) {
            field = searchBar
            notifyPropertyChanged(BR.searchBar)
            filterList(searchBar)
        }

    fun filterList(searchText : String) {
        var filteredProducts = ArrayList<ProductBindingModel>()
        for (product in listOfProducts) {
            if (product.productName.contains(searchText) || product.productDescription.contains(searchText)) {
                filteredProducts.add(product)
            }
        }
        listViewAdapter!!.updateList(filteredProducts)
    }

    @get:Bindable
    var isSwipeViewRefreshing: Boolean = false
        set(swipeViewRefreshing) {
            field = swipeViewRefreshing
            notifyPropertyChanged(BR.swipeViewRefreshing)
        }
}